﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Взмд с Клиентом и DAL
//Создаем методы, потом в конструкторе Настраиваем их для GridView.
//УКАЗАТЬ <asp:GridView ID="GridView1"  DataKeyNames="id"> !!!
public class BLL
{
	public BLL()
	{

	}

    public List<Person> GetAllPerson()
    {
        return DAL.GetAllPerson();
    }

    public Person GetPersonByID(int id)
    {
        return DAL.GetPersonID(id);
    }

    public bool DeletePersonByID(int id)
    {
        return DAL.DeletePerson(id);
    }

    public bool UpdatePersonByID(int id, string firstName, string lastName, string country, string city, string street, string house, string phone1, string phone2, bool isAdmin)
    {
        return DAL.UpdatePerson(new Person(id, firstName, lastName, country, city, street, house, phone1, phone2, isAdmin));
    }

    public bool CreatePerson( string firstName, string lastName, string country, string city, string street, string house, string phone1, string phone2, bool isAdmin)
    {
        return DAL.SavePerson(new Person(firstName, lastName, country, city, street, house, phone1, phone2, isAdmin));
    }
}
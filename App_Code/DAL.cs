﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public static class DAL
{
    static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    private static SqlConnection SqlCon //Св-во само определяет нужно ли создавать или исп-ть существующий
    {
        get
        {
            if (sqlCon == null)
                sqlCon = new SqlConnection(connectionString);
            return sqlCon;
        }
    }
    private static SqlConnection sqlCon;

    //#1 Получить все персоны из таблицы (БД)                   1 & 4
    public static List<Person> GetAllPerson()
    {
        List<Person> persons = new List<Person>();
        try
        {
            //1 запрос
            string query =
                string.Format("SELECT * from Person Order By FirstName");

            //2 Команда
            SqlCommand com = new SqlCommand(query, SqlCon);

            //3 откр соед
            SqlCon.Open();

            //4 reader. Если CommandBehavior.CloseConnection, тогда необязательно следить за SqlCon.State и закрывать (finally) Когда ридер закончит читать, соед автоматич закроется.!!!
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);  
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    persons.Add(
                        new Person( reader.GetInt32(0),
                                    reader["FirstName"].ToString(),
                                    reader["LastName"].ToString(),
                                    reader["Country"].ToString(),
                                    reader["City"].ToString(),
                                    reader["Street"].ToString(),
                                    reader["House"].ToString(),
                                    reader["Phone1"].ToString(),
                                    reader["Phone2"].ToString(),
                                    reader.GetBoolean(9)));
                }
            }
        }
        catch (Exception)
        {

        }
        finally
        {
            if (SqlCon.State == ConnectionState.Open)
                SqlCon.Close();
        }
        return persons;
    }

    //#4 получить персону по ее ID                              1 & 4
    public static Person GetPersonID(int id)
    {
        Person person = null;
        try
        {
            //1 запрос
            string query =
                string.Format("SELECT * From Person Where id = " + id);

            //2 Команда
            SqlCommand com = new SqlCommand(query, SqlCon);

            //3 откр соед
            SqlCon.Open();

            //4 reader. Если CommandBehavior.CloseConnection, тогда необязательно следить за SqlCon.State и закрывать (finally) Когда ридер закончит читать, соед автоматич закроется!!!
            SqlDataReader reader = com.ExecuteReader(CommandBehavior.CloseConnection);

            //5 Нужно получить одну строку (поэтому без while)! 
            if (reader.HasRows)
                reader.Read();
            person = new Person(reader.GetInt32(0),
                                  reader["FirstName"].ToString(),
                                  reader["LastName"].ToString(),
                                  reader["Country"].ToString(),
                                  reader["City"].ToString(),
                                  reader["Street"].ToString(),
                                  reader["House"].ToString(),
                                  reader["Phone1"].ToString(),
                                  reader["Phone2"].ToString(),
                                  reader.GetBoolean(9));
        }
        catch
        {

        }
        finally
        {
            if (SqlCon.State == ConnectionState.Open)
                SqlCon.Close();
        }
        return person;
    }

    //#2 Обновилась персона или нет.                            2 & 3
    //Где-то на высшем уровне формируется объект person        
    //и передается сюда в метод UpdatePerson, объектом person.
    public static bool UpdatePerson(Person person)
    {
        bool rez = false;

        try
        {
            //1 запрос
            string query =
                string.Format(
                    "Update Person SET FirstName = '{0}', LastName = '{1}', Country = '{2}', City = '{3}', Street = '{4}', House = '{5}', Phone1 = '{6}', Phone2 = '{7}', IsAdmin = '{8}' WHERE (id = {9})",
                    person.FirstName, person.LastName, person.Country, person.City, person.Street, person.House,
                    person.Phone1, person.Phone2, person.IsAdmin, person.ID);

            //2 Команда
            SqlCommand com = new SqlCommand(query, SqlCon);
            
            //3 откр соед
            SqlCon.Open();

            //4 выполнить команду (хранимая)
            if (com.ExecuteNonQuery() == 1) //ExecuteNonQuery() - возворащ 1 - если удачно, 0 - если нет!
                rez = true;
        }
        catch
        {

        }
        finally
        {
            //5 Закрыть соед.
            if (SqlCon.State == ConnectionState.Open)
                SqlCon.Close();
        }
        return rez;
    }

    //#3 Удалить!                                               2 & 3
    public static bool DeletePerson(int id)
    {
        bool rez = false;
        try
        {
            //1 запрос
            string query =
                string.Format("Delete From Person Where id = " + id);
            
            //2 Команда
            SqlCommand com = new SqlCommand(query, SqlCon);
            
            //3 откр соед
            SqlCon.Open();

            //4 выполнить команду (хранимая)
            if (com.ExecuteNonQuery() == 1) //ExecuteNonQuery() - возворащ 1 - если удачно, 0 - если нет!
                rez = true;
        }
        catch
        {

        }
        finally
        {
            //5 Закрыть соед.
            if (SqlCon.State == ConnectionState.Open)
                SqlCon.Close();
        }
        return rez;
    }

    public static bool SavePerson(Person person)
    {
        return false;
    }
}
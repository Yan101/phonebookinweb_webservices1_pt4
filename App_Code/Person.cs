﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Persons
/// </summary>
public class Person
{
    public int ID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Country { get; set; }
    public string City { get; set; }
    public string Street{ get; set; }
    public string House{ get; set; }
    public string Phone1 { get; set; }
    public string Phone2 { get; set; }
    public bool IsAdmin { get; set; }

	public Person()    {   }

    public Person(int id, string firstName, string lastName, string country, string city, string street, string house, string phone1, string phone2, bool isAdmin)
    {
        this.ID = id;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Country = country;
        this.City = city;
        this.Street = street;
        this.House = house;
        this.Phone1 = phone1;
        this.Phone2 = phone2;
        this.IsAdmin = isAdmin;
    }

    public Person(string firstName, string lastName, string country, string city, string street, string house, string phone1, string phone2, bool isAdmin)
        : this(-1, firstName, lastName, country, city, street, house, phone1, phone2, isAdmin)
    {
        
    }
}
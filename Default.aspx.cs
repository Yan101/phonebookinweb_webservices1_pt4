﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ru.cbr.www;

public partial class _Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        ru.cbr.www.DailyInfo di = new ru.cbr.www.DailyInfo();
        DataSet ds = di.GetCursOnDate(DateTime.Today);
        GridView1.DataSource = ds;
        GridView1.DataBind();
        Response.Write(ds.Tables[0].Rows[5][4].ToString() + " -> " + ds.Tables[0].Rows[5]["Vcurs"].ToString());
    }
}